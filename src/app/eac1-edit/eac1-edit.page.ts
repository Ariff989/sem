import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-eac1-edit',
  templateUrl: './eac1-edit.page.html',
  styleUrls: ['./eac1-edit.page.scss'],
})
export class EAC1EditPage implements OnInit {

  id: any;
  building: any;
  data: any;
  month: any;
  electric_consump: any;
  eac_count: number;

  constructor(public storage: Storage, private activatedRoute: ActivatedRoute, private navCtrl: NavController) {
    this.storage.create();

  }

  ionViewWillEnter() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    // this.storage.get('eac-1-' + this.id).then((val: any) => {

    //   this.building = val[0].building;
    //   this.month = val[0].month;
    //   this.electric_consump = val[0].electric_consump;
    // });

    this.storage.get('eac-1-count').then((val) => {
      console.log(val);

      if (!val) {
        val = 0;
      }

      for (let i = 0; i < val; i++) {

        this.storage.get('eac-1-' + i).then((val) => {

          if (val && val[0].id == this.id) {

            this.building = val[0].building;
            this.month = val[0].month;
            this.electric_consump = val[0].electric_consump;
          }

          console.log(val);

        });
      }
    });
  }

  ngOnInit() {
  }

  updateBuilding() {
    if (this.building && this.month && this.electric_consump) {

      var data = [
        {
          id: this.id,
          building: this.building,
          month: this.month,
          electric_consump: this.electric_consump,
        }
      ];

      this.storage.get('eac-1-count').then((val) => {
        console.log(val);
  
        if (!val) {
          val = 0;
        }
  
        console.log(this.id);
        
        for (let i = 0; i < val; i++) {
  
          this.storage.get('eac-1-' + i).then((val) => {
  
            if (val && val[0].id == this.id) {
  
              this.storage.set('eac-1-' + i, data);
  
              this.navCtrl.navigateBack(['/eac1'])
            }
  
            console.log(val);
  
          });
        }
      });

      this.navCtrl.navigateBack(['/eac1'])

    }
  }

  deleteBuilding() {
    this.storage.get('eac-1-count').then((val) => {
      console.log(val);

      if (!val) {
        val = 0;
      }

      console.log(this.id);
      
      for (let i = 0; i < val; i++) {

        this.storage.get('eac-1-' + i).then((val) => {

          if (val && val[0].id == this.id) {

            this.storage.remove('eac-1-' + i);

            this.navCtrl.navigateBack(['/eac1'])
          }

          console.log(val);

        });
      }
    });
  }

}
