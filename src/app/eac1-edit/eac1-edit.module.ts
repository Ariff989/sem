import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EAC1EditPageRoutingModule } from './eac1-edit-routing.module';

import { EAC1EditPage } from './eac1-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EAC1EditPageRoutingModule
  ],
  declarations: [EAC1EditPage]
})
export class EAC1EditPageModule {}
