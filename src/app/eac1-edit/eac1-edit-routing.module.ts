import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EAC1EditPage } from './eac1-edit.page';

const routes: Routes = [
  {
    path: '',
    component: EAC1EditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EAC1EditPageRoutingModule {}
