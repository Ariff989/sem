import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'eac1',
    loadChildren: () => import('./eac1/eac1.module').then( m => m.EAC1PageModule)
  },
  {
    path: 'eac1-add',
    loadChildren: () => import('./eac1-add/eac1-add.module').then( m => m.EAC1AddPageModule)
  },
  {
    path: 'eac1-edit/:id',
    loadChildren: () => import('./eac1-edit/eac1-edit.module').then( m => m.EAC1EditPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
