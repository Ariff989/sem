import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EAC1Page } from './eac1.page';

const routes: Routes = [
  {
    path: '',
    component: EAC1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EAC1PageRoutingModule {}
