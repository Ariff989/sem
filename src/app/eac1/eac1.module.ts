import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EAC1PageRoutingModule } from './eac1-routing.module';

import { EAC1Page } from './eac1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EAC1PageRoutingModule
  ],
  declarations: [EAC1Page]
})
export class EAC1PageModule {}
