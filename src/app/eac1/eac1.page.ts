import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-eac1',
  templateUrl: './eac1.page.html',
  styleUrls: ['./eac1.page.scss'],
})
export class EAC1Page implements OnInit {

  months: any = [];
  buildings: any = [];
  eac_count: any;

  constructor(public storage: Storage) {
    this.storage.create();

    this.months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
  }

  doRefresh(event) {
    this.ionViewWillEnter();

    setTimeout(() => {
      event.target.complete();
    }, 500);
  }

  ionViewWillEnter() {
    this.buildings = [];

    this.storage.get('eac-1-count').then((val) => {
      console.log(val);

      if (!val) {
        val = 0;
      }
      console.log(val);

      for (let i = 0; i < val; i++) {

        this.storage.get('eac-1-' + i).then((val) => {
          console.log(val);

          if (val) {
            this.buildings.push(val[0]);
          }
          
          console.log(this.buildings);

        });
      }
    });
  }

  ngOnInit() {
    this.storage.create();
  }

}
