import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/home', icon: 'home' },
    { title: 'Tips', url: '/folder/tips', icon: 'albums' },
    { title: 'EAC 1', url: '/eac1', icon: 'business' },
    { title: 'EAC 2', url: '/folder/eac2', icon: 'business' },
    { title: 'EAC 3', url: '/folder/eac3', icon: 'business' },
    { title: 'About', url: '/folder/about', icon: 'alert-circle' },
    { title: 'Logout', url: '/folder/logout', icon: 'log-out' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
