import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EAC1AddPageRoutingModule } from './eac1-add-routing.module';

import { EAC1AddPage } from './eac1-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EAC1AddPageRoutingModule
  ],
  declarations: [EAC1AddPage]
})
export class EAC1AddPageModule {}
