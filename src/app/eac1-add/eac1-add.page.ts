import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-eac1-add',
  templateUrl: './eac1-add.page.html',
  styleUrls: ['./eac1-add.page.scss'],
})
export class EAC1AddPage implements OnInit {

  id:any;
  building:any;
  month:any;
  electric_consump:any;
  eac_count:any;

  constructor(public storage: Storage, private navCtrl: NavController) { 
    this.storage.create();
  }

  ionViewWillEnter()
  {
    this.storage.get('eac-1-count').then((val)=> {
      if (!val) {
        val = 0;
      }
      this.eac_count = val;
    });
  }

  ngOnInit() {
  }

  storeBuilding()
  {
    // this.storage.clear();

    if (this.building && this.month && this.electric_consump) {

      var data = [
        {
          id: this.eac_count,
          building: this.building,
          month: this.month,
          electric_consump: this.electric_consump,
        }
      ];

      this.storage.set('eac-1-' + this.eac_count, data);

      this.storage.set('eac-1-count', this.eac_count + 1);

      this.navCtrl.navigateBack(['/eac1'])
         
    }
  }

}
