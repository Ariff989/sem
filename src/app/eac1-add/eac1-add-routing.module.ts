import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EAC1AddPage } from './eac1-add.page';

const routes: Routes = [
  {
    path: '',
    component: EAC1AddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EAC1AddPageRoutingModule {}
